import {inject} from '@loopback/core';
import {juggler} from '@loopback/repository';
import * as config from './mongo-conecction.datasource.json';

export class MongoConecctionDataSource extends juggler.DataSource {
  static dataSourceName = 'mongoConecction';

  constructor(
    @inject('datasources.config.mongoConecction', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
