import {Model, model, property, Entity} from '@loopback/repository';

@model()
export class Usuarios extends Entity {
  @property({
    type: 'string',
    id: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  usuario: string;

  @property({
    type: 'string',
    required: true,
  })
  nombres: string;

  @property({
    type: 'string',
  })
  contrasena: string;

  @property({
    type: 'string',
    required: true,
  })
  correo: string;

  constructor(data?: Partial<Usuarios>) {
    super(data);
  }
}
