import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  del,
  requestBody,
} from '@loopback/rest';
import {Usuarios} from '../models';
import {UsuariosRepository} from '../repositories';

export class UsuariosController {
  constructor(
    @repository(UsuariosRepository)
    public usuariosRepository : UsuariosRepository,
  ) {}

  @post('/usuarios', {
    responses: {
      '200': {
        description: 'Usuarios model instance',
        content: {'application/json': {schema: {'x-ts-type': Usuarios}}},
      },
    },
  })
  async create(@requestBody() usuarios: Usuarios): Promise<Usuarios> {
    return await this.usuariosRepository.create(usuarios);
  }

  @get('/usuarios/count', {
    responses: {
      '200': {
        description: 'Usuarios model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Usuarios)) where?: Where,
  ): Promise<Count> {
    return await this.usuariosRepository.count(where);
  }

  @get('/usuarios', {
    responses: {
      '200': {
        description: 'Array of Usuarios model instances',
        content: {
          'application/json': {
            schema: {type: 'array', items: {'x-ts-type': Usuarios}},
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Usuarios)) filter?: Filter,
  ): Promise<Usuarios[]> {
    return await this.usuariosRepository.find(filter);
  }

  @patch('/usuarios', {
    responses: {
      '200': {
        description: 'Usuarios PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody() usuarios: Usuarios,
    @param.query.object('where', getWhereSchemaFor(Usuarios)) where?: Where,
  ): Promise<Count> {
    return await this.usuariosRepository.updateAll(usuarios, where);
  }

  @get('/usuarios/{id}', {
    responses: {
      '200': {
        description: 'Usuarios model instance',
        content: {'application/json': {schema: {'x-ts-type': Usuarios}}},
      },
    },
  })
  async findById(@param.path.string('id') id: string): Promise<Usuarios> {
    return await this.usuariosRepository.findById(id);
  }

  @patch('/usuarios/{id}', {
    responses: {
      '204': {
        description: 'Usuarios PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody() usuarios: Usuarios,
  ): Promise<void> {
    await this.usuariosRepository.updateById(id, usuarios);
  }

  @del('/usuarios/{id}', {
    responses: {
      '204': {
        description: 'Usuarios DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.usuariosRepository.deleteById(id);
  }
}
