import {DefaultCrudRepository, juggler} from '@loopback/repository';
import {Usuarios} from '../models';
import {MongoConecctionDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UsuariosRepository extends DefaultCrudRepository<
  Usuarios,
  typeof Usuarios.prototype.id
> {
  constructor(
    @inject('datasources.mongoConecction') dataSource: MongoConecctionDataSource,
  ) {
    super(Usuarios, dataSource);
  }
}
