export default (message, title = 'Ups!', options = { actionAlert: false }) => {
    const notificacion = new Promise((resolve, reject) => {
        console.log('options :', options);
        if (options.actionAlert) {
            confirm({
                title,
                message,
                okButtonText: 'OK',
                cancelButtonText: 'SALIR'
            }).then((res) => {
                if (res){
                    resolve()
                }
            }).catch(err => reject(err));
        } else {
            alert({
                title,
                message,
                okButtonText: options.OK,
            }).then(() => {
                resolve()
            }).catch(err => reject(err));
        }   
    });
    return notificacion;
}