import { LOGIN_USER, API_URL } from './constants';

export const LoginRequest = (user, pass) => {
    const promise = new Promise((resolve, reject) => {
        const PASSWORD = LOGIN_USER.replace('usuario', 'contrasena')
        fetch(`${API_URL}${LOGIN_USER}${user}&${PASSWORD}${pass}`, {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(result => {
            return result.json();
        }).then(response => {
            if (response.length) {
                resolve(true)
            } else {
                resolve(false)
            }
        })
        .catch(err => reject(err));
    });

    return promise;
}

export const AddUser = (user) => {
    const promise = new Promise((resolve, reject) => {
        const USER = LOGIN_USER.split('?')[0];
        fetch(`${API_URL}${USER}`, {
            method: 'POST',
            body: JSON.stringify(user),
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(result => {
            return result.json();
        }).then(response => {
            resolve(response);
        })
        .catch(err => reject(err));
    });

    return promise;
}


export const getUsers = () => {
    const promise = new Promise((resolve, reject) => {
        const USER = LOGIN_USER.split('?')[0];
        fetch(`${API_URL}${USER}`, {
            method: 'GET',
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(result => {
            return result.json();
        }).then(response => {
            if (response.length) {
                resolve(response)
            } else {
                resolve(false)
            }
        })
        .catch(err => reject(err));
    });

    return promise;
}

export const deleteUser = (id) => {
    const promise = new Promise((resolve, reject) => {
        const USER = LOGIN_USER.split('?')[0];
        fetch(`${API_URL}${USER}/${id}`, {
            method: 'DELETE',
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(() => {
            resolve()
        })
        .catch(err => reject(err));
    });

    return promise;
}

export const updateUser = (user) => {
    const promise = new Promise((resolve, reject) => {
        const USER = LOGIN_USER.split('?')[0];
        fetch(`${API_URL}${USER}/${user.id}`, {
            method: 'PATCH',
            body: JSON.stringify(user),
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(() => {
            resolve()
        })
        .catch(err => reject(err));
    });

    return promise;
}

